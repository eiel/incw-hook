package document

import "time"

type Token struct {
	AccessToken  string    `firestore:"access_token,omitempty"`
	TokenType    string    `firestore:"token_type,omitempty"`
	RefreshToken string    `firestore:"refresh_token,omitempty"`
	Expiry       time.Time `firestore:"expiry,omitempty"`
	OwnerID      UserID    `firestore:"owner_id,omitempty"`
}

type UserID = string

func (*Token) Collection() string {
	return "tokens"
}

func (t *Token) Key() string {
	return t.OwnerID
}
