package document

import (
	"testing"
)

func TestTokenCollectionName_tokens(t *testing.T) {
	expected := "tokens"
	token := &Token{}
	actual := token.Collection()
	if expected != actual {
		t.Errorf("expected: %v, actual: %v", expected, actual)
	}
}

func TestTokenIDisOwnerID(t *testing.T) {
	expected := "Owner"
	token := &Token{
		OwnerID: "Owner",
	}
	actual := token.Key()
	if expected != actual {
		t.Errorf("expected: %v, actual: %v", expected, actual)
	}
}
