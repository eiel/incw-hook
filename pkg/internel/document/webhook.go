package document

type WebHook struct {
	HookID   HookID `firestore:"hook_id,omitempty"`
	OwenerID UserID `firestore:"owner_id,omitempty"`
	RoomID   RoomID `firestore:"room_id,omitempty"`
}

type HookID = string
type RoomID = string

func (*WebHook) Collection() string {
	return "web_hooks"
}

func (t *WebHook) Key() string {
	return t.HookID
}
