package internel

import (
	"context"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go/v4"
)

type Document interface {
	// Collection name
	Collection() string
	// Document name
	Key() string
}

func InitStore(ctx context.Context, projectID string) (*firestore.Client, error) {
	conf := &firebase.Config{ProjectID: projectID}
	app, err := firebase.NewApp(ctx, conf)
	if err != nil {
		return nil, err
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func SetDoc(ctx context.Context, client *firestore.Client, doc Document) error {
	_, err := client.Collection(doc.Collection()).Doc(doc.Key()).Set(ctx, doc)
	return err
}

func GetDoc[t Document](ctx context.Context, client *firestore.Client, doc t) error {
	data, err := client.Collection(doc.Collection()).Doc(doc.Key()).Get(ctx)
	if err != nil {
		return err
	}
	return data.DataTo(doc)
}
