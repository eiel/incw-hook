package handler

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strings"

	"gitlab.com/eiel/incw-hook/pkg/chatwork"
	"gitlab.com/eiel/incw-hook/pkg/oauth"
	oacw "gitlab.com/eiel/incw-hook/pkg/oauth/chatwork"
	"golang.org/x/oauth2"
)

func serverInternelError(err error, w http.ResponseWriter) {
	log.Fatalln(err)
	w.WriteHeader(http.StatusInternalServerError)
}

type IncomingWebhook struct {
	Conf         *oauth2.Config
	TokenStore   oacw.TokenStore
	WebHookStore oacw.WebHookStore
}

func (h IncomingWebhook) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	fmt.Fprintf(w, "accept\n")
	log.Println("retrive web_hook from store...")
	path := r.URL.Path
	hookID := strings.TrimPrefix(path, "/incoming/")
	wh, err := h.WebHookStore.Get(ctx, hookID)
	if err != nil {
		serverInternelError(err, w)
		return
	}

	log.Println("retrive token from store...")
	tkn, err := h.TokenStore.Token(ctx, wh.OwenerID)
	if err != nil {
		serverInternelError(err, w)
		return
	}

	log.Println("refresh chwork access token...")
	tk, err := oauth.Refresh(ctx, tkn.RefreshToken, h.Conf)
	if err != nil {
		serverInternelError(err, w)
		return
	}

	fmt.Fprintf(w, "start post\n")
	r.ParseForm()
	body := fmt.Sprintf("%v\n\n--\n[sended by incw]", r.PostForm.Get("body"))

	_, err = chatwork.PostMessage(tk.AccessToken, wh.RoomID, body)
	if err != nil {
		log.Fatalln(err)
		// fail post message error
		return
	}
	log.Println("finish")
	fmt.Fprintf(w, "success\n")
}
