package handler

import "os"

type OAuth struct {
	ClientID     string
	ClientSecret string
}

type OAuthServices struct {
	Chatwork OAuth
}

type GCP struct {
	ProjectID string
}

type Env struct {
	OAuth OAuthServices
	GCP   GCP
}

var env *Env

func GetEnv() *Env {
	if env != nil {
		return env
	}
	env = &Env{
		OAuth: OAuthServices{
			Chatwork: OAuth{
				ClientID:     os.Getenv("CHATWORK_OAUTH2_CLIENT_ID"),
				ClientSecret: os.Getenv("CHATWORK_OAUTH2_CLIENT_SECRET"),
			},
		},
		GCP: GCP{
			os.Getenv("GOOGLE_CLOUD_PROJECT_ID"),
		},
	}
	return env
}
