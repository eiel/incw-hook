package handler

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/eiel/incw-hook/pkg/internel/document"
	"gitlab.com/eiel/incw-hook/pkg/oauth"
	oacw "gitlab.com/eiel/incw-hook/pkg/oauth/chatwork"
	"golang.org/x/oauth2"
)

// Chatwork OAuth entry point.
// Redirect Chatwork OAuth concent page.
type ChatworkGetAuthorizationCode struct {
	Conf *oauth2.Config
}

func (h ChatworkGetAuthorizationCode) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// TODO: params is temporaly state. should create access uniq string
	authUrl := h.Conf.AuthCodeURL("state")
	w.Header().Set("Location", authUrl)
	w.WriteHeader(http.StatusTemporaryRedirect)
}

type ChatworkCallbackAuthorizationCode struct {
	Conf       *oauth2.Config
	TokenStore oacw.TokenStore
}

func (h ChatworkCallbackAuthorizationCode) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	userID := os.Getenv("TEMP_USER_ID")
	ctx := context.Background()
	q := r.URL.Query()
	code := q.Get("code")
	state := q.Get("state")

	if state != "state" {
		serverInternelError(fmt.Errorf("unmatch state error"), w)
		return
	}
	token, err := oauth.Exchange(ctx, code, h.Conf)
	if err != nil {
		serverInternelError(err, w)
		return
	}

	tkn := document.Token{
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
		TokenType:    token.TokenType,
		Expiry:       token.Expiry,
		OwnerID:      userID,
	}

	h.TokenStore.SetToken(ctx, &tkn)
	fmt.Fprintf(w, "success")
}
