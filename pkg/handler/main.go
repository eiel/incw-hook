package handler

import (
	"context"
	"net/http"
	"strings"

	"cloud.google.com/go/firestore"
	"gitlab.com/eiel/incw-hook/pkg/internel"
	oacw "gitlab.com/eiel/incw-hook/pkg/oauth/chatwork"
	"golang.org/x/oauth2"
)

func setup() (*oauth2.Config, *firestore.Client, error) {
	var env = GetEnv()
	cw := &env.OAuth.Chatwork
	scopes := []string{"offline_access", "rooms.messages:write"}
	conf := oacw.MakeConfig(cw.ClientID, cw.ClientSecret, scopes)

	ctx := context.Background()
	client, err := internel.InitStore(ctx, env.GCP.ProjectID)
	return conf, client, err
}

type MainHandler struct {
	client                            *firestore.Client
	chatworkGetAuthorizationCode      ChatworkGetAuthorizationCode
	chatworkCallbackAuthorizationCode ChatworkCallbackAuthorizationCode
	incomingWebHook                   IncomingWebhook
}

func (h MainHandler) Close() {
	if h.client != nil {
		h.client.Close()
	}
}

func (h MainHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	var handler http.Handler = http.NotFoundHandler()
	switch true {
	case path == "/oauth/chatwork":
		handler = h.chatworkGetAuthorizationCode
	case path == "/oauth/chatwork/callback":
		handler = h.chatworkCallbackAuthorizationCode
	case strings.HasPrefix(path, "/incoming"):
		if r.Method == "POST" {
			handler = h.incomingWebHook
		}
	}
	handler.ServeHTTP(w, r)
}

func MakeMain() (*MainHandler, error) {
	conf, client, err := setup()
	if err != nil {
		return nil, err
	}
	var tokenStore = &oacw.TokenFireStore{Client: client}
	var webHookStore = &oacw.WebHookFireStore{Client: client}

	handler := MainHandler{
		client:                       client,
		chatworkGetAuthorizationCode: ChatworkGetAuthorizationCode{Conf: conf},
		chatworkCallbackAuthorizationCode: ChatworkCallbackAuthorizationCode{
			Conf:       conf,
			TokenStore: tokenStore,
		},
		incomingWebHook: IncomingWebhook{
			Conf:         conf,
			TokenStore:   tokenStore,
			WebHookStore: webHookStore,
		},
	}
	return &handler, nil
}
