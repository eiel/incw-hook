package chatwork

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

type AccessToken = string
type RoomID = string

func Me(tk AccessToken) (*http.Response, error) {
	base := "https://api.chatwork.com/v2"
	endpoint := "/me"
	url := strings.Join([]string{base, endpoint}, "")

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	header := fmt.Sprintf("Bearer %v", tk)
	req.Header.Add("authorization", header)
	htc := &http.Client{}
	return htc.Do(req)
}

func PostMessage(at AccessToken, roomID RoomID, body string) (*http.Response, error) {
	base := "https://api.chatwork.com/v2"
	endpoint := fmt.Sprintf("%s/rooms/%s/messages", base, roomID)

	value := url.Values{
		"body": []string{body},
	}

	req, err := http.NewRequest("POST", endpoint, strings.NewReader(value.Encode()))
	if err != nil {
		return nil, err
	}

	header := fmt.Sprintf("Bearer %v", at)
	req.Header.Add("authorization", header)
	htc := &http.Client{}
	return htc.Do(req)
}
