package chatwork

import "golang.org/x/oauth2"

var Endpoint = oauth2.Endpoint{
	AuthURL:   "https://www.chatwork.com/packages/oauth2/login.php",
	TokenURL:  "https://oauth.chatwork.com/token",
	AuthStyle: oauth2.AuthStyleInHeader,
}

func MakeConfig(clientID, clientSecret string, scopes []string) *oauth2.Config {
	return &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Scopes:       scopes,
		Endpoint:     Endpoint,
	}
}
