package chatwork

import (
	"context"

	"cloud.google.com/go/firestore"
	"gitlab.com/eiel/incw-hook/pkg/internel"
	"gitlab.com/eiel/incw-hook/pkg/internel/document"
)

type WebHookStore interface {
	Get(ctx context.Context, id document.HookID) (*document.WebHook, error)
	Set(ctx context.Context, token *document.WebHook) error
}

type WebHookFireStore struct {
	Client *firestore.Client
}

func (s *WebHookFireStore) Get(ctx context.Context, id OwnerID) (*document.WebHook, error) {
	hook := document.WebHook{
		HookID: id,
	}
	err := internel.GetDoc(ctx, s.Client, &hook)
	return &hook, err
}

func (s *WebHookFireStore) Set(ctx context.Context, hook *document.WebHook) error {
	return internel.SetDoc(ctx, s.Client, hook)
}
