package chatwork

import (
	"context"

	"cloud.google.com/go/firestore"
	"gitlab.com/eiel/incw-hook/pkg/internel"
	"gitlab.com/eiel/incw-hook/pkg/internel/document"
)

type TokenStore interface {
	Token(ctx context.Context, id OwnerID) (*document.Token, error)
	SetToken(ctx context.Context, token *document.Token) error
}

type TokenFireStore struct {
	Client *firestore.Client
}

type OwnerID = string

func (s *TokenFireStore) Token(ctx context.Context, id OwnerID) (*document.Token, error) {
	tkn := document.Token{
		OwnerID: id,
	}
	err := internel.GetDoc(ctx, s.Client, &tkn)
	return &tkn, err
}

func (s *TokenFireStore) SetToken(ctx context.Context, tkn *document.Token) error {
	return internel.SetDoc(ctx, s.Client, tkn)
}
