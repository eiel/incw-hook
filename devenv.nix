{ pkgs, ... }:

{
  # https://devenv.sh/basics/
  env = {
    GREET = "devenv";
    # following env define devenv.local.nix
    # CHATWORK_OAUTH2_CLIENT_ID =  "";
    # CHATWORK_OAUTH2_CLIENT_SECRET = "";
    # GOOGLE_CLOUD_PROJECT_ID = "incw-hook";
    # TEMP_USER_ID = "";
  };

  # https://devenv.sh/packages/
  packages = [
    pkgs.git
    pkgs.google-cloud-sdk
  ];

  # https://devenv.sh/scripts/
  scripts.hello.exec = "echo hello from $GREET";

  enterShell = ''
    hello
    git --version
  '';

  # https://devenv.sh/languages/
  # languages.nix.enable = true;
  # https
  # https://github.com/cachix/devenv/blob/main/src/modules/languages/go.nix
  languages.go.enable = true;

  # https://devenv.sh/pre-commit-hooks/
  # pre-commit.hooks.shellcheck.enable = true;
  pre-commit.hooks = {
    gofmt.enable = true;
    gotest.enable = true;
  };
  # https://devenv.sh/processes/
  # processes.ping.exec = "ping example.com";
  processes.server.exec = "go run cmd/server/main.go";

  # See full reference at https://devenv.sh/reference/options/
}
