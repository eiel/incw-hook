package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/eiel/incw-hook/pkg/chatwork"
	"gitlab.com/eiel/incw-hook/pkg/internel"
	"gitlab.com/eiel/incw-hook/pkg/oauth"
	oacw "gitlab.com/eiel/incw-hook/pkg/oauth/chatwork"
)

func main() {
	projectID := os.Getenv("GOOGLE_CLOUD_PROJECT_ID")
	userID := os.Getenv("TEMP_USER_ID")

	ctx := context.Background()
	client, err := internel.InitStore(ctx, projectID)
	if err != nil {
		log.Fatalln(err)
		os.Exit(1)
	}
	defer client.Close()

	store := oacw.TokenFireStore{Client: client}

	log.Println("retrive token from store...")
	tkn, err := store.Token(ctx, userID)
	if err != nil {
		log.Fatalln(err)
		os.Exit(1)
	}

	log.Println("refresh chwork access token...")
	clientID := os.Getenv("CHATWORK_OAUTH2_CLIENT_ID")
	clientSecret := os.Getenv("CHATWORK_OAUTH2_CLIENT_SECRET")
	conf := oacw.MakeConfig(clientID, clientSecret, []string{"offline_access", "rooms.messages:write"})

	tk, err := oauth.Refresh(ctx, tkn.RefreshToken, conf)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	roomID := os.Getenv("CHATWORK_ROOM_ID")
	resp, err := chatwork.PostMessage(tk.AccessToken, roomID, "hello")
	if err != nil {
		log.Fatalln(err)
		os.Exit(1)
	}

	log.Println(resp.Status)
	log.Println(resp.Header)

	bs, _ := io.ReadAll(resp.Body)
	fmt.Println(string(bs))
}
