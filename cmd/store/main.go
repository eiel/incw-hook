package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"cloud.google.com/go/firestore"
	"gitlab.com/eiel/incw-hook/pkg/internel"
)

type City struct {
	Name       string   `firestore:"name,omitempty"`
	State      string   `firestore:"state,omitempty"`
	Country    string   `firestore:"country,omitempty"`
	Capital    bool     `firestore:"capital,omitempty"`
	Population int64    `firestore:"population,omitempty"`
	Regions    []string `firestore:"regions,omitempty"`
}

func addDocAsEntity(ctx context.Context, client *firestore.Client) error {
	city := City{
		Name:    "Los Angeles",
		Country: "USA",
	}
	_, err := client.Collection("cities").Doc("LA").Set(ctx, city)
	if err != nil {
		// Handle any errors in an appropriate way, such as returning them.
		log.Printf("An error has occurred: %s", err)
	}

	return err
}

func docAsEntity(ctx context.Context, client *firestore.Client, doc string) (*City, error) {
	dsnap, err := client.Collection("cities").Doc(doc).Get(ctx)
	if err != nil {
		return nil, err
	}
	var c City
	dsnap.DataTo(&c)
	return &c, nil
}

func main() {
	ctx := context.Background()
	projectID := os.Getenv("GOOGLE_CLOUD_PROJECT_ID")
	client, err := internel.InitStore(ctx, projectID)
	if err != nil {
		log.Fatalln(err)
		return
	}
	defer client.Close()

	city, err := docAsEntity(ctx, client, "LA")
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("Document data: %#v\n", city)
}
