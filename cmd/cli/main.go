package main

import (
	"context"
	"fmt"
	"log"
	"net/url"
	"os"

	"gitlab.com/eiel/incw-hook/pkg/oauth"
	"gitlab.com/eiel/incw-hook/pkg/oauth/chatwork"
)

func main() {
	ctx := context.Background()
	clientID := os.Getenv("CHATWORK_OAUTH2_CLIENT_ID")
	clientSecret := os.Getenv("CHATWORK_OAUTH2_CLIENT_SECRET")
	conf := chatwork.MakeConfig(
		clientID,
		clientSecret,
		[]string{"rooms.messages:write"},
	)

	authUrl := conf.AuthCodeURL("state")

	fmt.Printf("Visit the URL for the auth dialog: %v\n", authUrl)

	// Use the authorization code that is pushed to the redirect
	// URL. Exchange will do the handshake to retrieve the
	// initial access token. The HTTP Client returned by
	// conf.Client will refresh the token as necessary.
	var callbackUrl string
	if _, err := fmt.Scan(&callbackUrl); err != nil {
		log.Fatal(err)
	}

	var u *url.URL
	var err error
	if u, err = url.ParseRequestURI(callbackUrl); err != nil {
		log.Fatal(err)
	}

	query := u.Query()
	code := query.Get("code")

	fmt.Printf("code: %v\n", code)
	tok, err := oauth.Exchange(ctx, code, conf)
	if err != nil {
		log.Fatal(err)
	}

	// client := conf.Client(ctx, tok)
	fmt.Printf("%v", tok)
}
