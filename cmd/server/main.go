package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/eiel/incw-hook/pkg/handler"
)

func main() {
	handler, err := handler.MakeMain()
	defer handler.Close()
	if err != nil {
		log.Fatalln(err)
		os.Exit(1)
	}

	log.Print("Start server: https://localhost:8080")
	err = http.ListenAndServe(":8080", handler)
	if err != nil {
		log.Fatalln(err)
		return
	}
}
